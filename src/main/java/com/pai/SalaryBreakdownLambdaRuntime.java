package com.pai;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import io.micronaut.function.aws.runtime.AbstractMicronautLambdaRuntime;

import java.net.MalformedURLException;
import java.util.LinkedHashMap;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import io.micronaut.core.annotation.Nullable;

import java.util.List;
public class SalaryBreakdownLambdaRuntime extends AbstractMicronautLambdaRuntime<APIGatewayProxyRequestEvent,
        APIGatewayProxyResponseEvent, LinkedHashMap<String, Object>, List<SalaryBreakdown>> {

    public static void main(String[] args) {
        try {
            new SalaryBreakdownLambdaRuntime().run(args);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Nullable
    protected RequestHandler<LinkedHashMap<String, Object>, List<SalaryBreakdown>> createRequestHandler(String... args) {
        return new SalaryBreakdownRequestHandler();
    }
}
