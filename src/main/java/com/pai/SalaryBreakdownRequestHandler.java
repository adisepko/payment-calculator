package com.pai;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import jakarta.inject.Inject;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;

@Introspected
public class SalaryBreakdownRequestHandler extends MicronautRequestHandler<LinkedHashMap<String, Object>, List<SalaryBreakdown>> {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    private PaymentCalculationService paymentCalculationService;

    @Override
    public List<SalaryBreakdown> execute(LinkedHashMap<String, Object> input) {
        Object o = ((Map<?, ?>) input.get("queryStringParameters")).get("employee");
        try {
            Employee employee = objectMapper.readValue((String) o, Employee.class);
            return paymentCalculationService.calculate(employee);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
