package com.pai;

import jakarta.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class PaymentCalculationService {
    private static final Logger logger = LoggerFactory.getLogger(PaymentCalculationService.class);
    public static final BigDecimal LOWER_TAX_RATE = BigDecimal.valueOf(0.17);
    public static final BigDecimal HIGHER_TAX_RATE = BigDecimal.valueOf(0.32);
    private static final BigDecimal HEALTH_INSURANCE_RATE = BigDecimal.valueOf(0.09);
    public static final BigDecimal TAX_FREE_ALLOWANCE = BigDecimal.valueOf(425);

    public List<SalaryBreakdown> calculate(Employee employee) {
        logger.debug("salary: {}, isWorkingInPlaceOfResidence: {}, isOver26: {}, taxReliefEnabled: {}", employee.getSalary(),
                employee.isWorkingInPlaceOfResidence(), employee.isOver26(), employee.isTaxReliefEnabled());
        BigDecimal taxDeductibleCost = employee.isWorkingInPlaceOfResidence() ? BigDecimal.valueOf(250) : BigDecimal.valueOf(300);
        return employee.getSalary().stream()
                .map(grossSalary -> mapToSalaryBreakdown(grossSalary,
                        taxDeductibleCost, employee.isTaxReliefEnabled(), employee.isOver26()))
                .collect(Collectors.toList());

    }

    private SalaryBreakdown mapToSalaryBreakdown(BigDecimal grossSalary, BigDecimal taxDeductibleCost,
                                                 boolean isTaxReliefEnabled, boolean isOver26) {
        var socialInsuranceContribution = SocialInsuranceContribution.calculate(grossSalary);
        BigDecimal healthInsuranceBase = grossSalary.subtract(socialInsuranceContribution.getSocialInsuranceContributionValue());
        BigDecimal healthInsurance = healthInsuranceBase.multiply(HEALTH_INSURANCE_RATE).setScale(2, RoundingMode.HALF_UP);
        BigDecimal thisYearSalary = BigDecimal.ZERO;
        BigDecimal taxPrepayment = isOver26
                ? calculateTaxPrepayment(grossSalary, taxDeductibleCost, healthInsuranceBase, isTaxReliefEnabled, thisYearSalary)
                : BigDecimal.ZERO;
        BigDecimal netSalary = grossSalary
                .subtract(socialInsuranceContribution.getSocialInsuranceContributionValue())
                .subtract(healthInsurance)
                .subtract(taxPrepayment.setScale(2, RoundingMode.HALF_UP));
        return new SalaryBreakdown(socialInsuranceContribution, healthInsurance,
                grossSalary, taxDeductibleCost, taxPrepayment, netSalary);
    }

    private BigDecimal calculateTaxPrepayment(BigDecimal grossSalary, BigDecimal taxDeductibleCost,
                                              BigDecimal healthInsuranceBase, boolean taxReliefEnabled,
                                              BigDecimal thisYearSalary) {
        BigDecimal taxBase = healthInsuranceBase.subtract(taxDeductibleCost).setScale(2, RoundingMode.HALF_UP);
        BigDecimal taxRelief = taxReliefEnabled ? calculateTaxRelief(grossSalary) : BigDecimal.ZERO;
        logger.debug("{}-{}x{}-{}", taxBase, taxRelief.doubleValue(), LOWER_TAX_RATE, TAX_FREE_ALLOWANCE);
        BigDecimal tax = taxBase
                .subtract(taxRelief)
                .multiply(LOWER_TAX_RATE)
                .subtract(TAX_FREE_ALLOWANCE)
                .setScale(0, RoundingMode.HALF_UP);
        return tax.compareTo(BigDecimal.ZERO) > 0 ? tax : BigDecimal.ZERO;
    }

    private BigDecimal calculateTaxRelief(BigDecimal grossSalary) {
        var v = grossSalary.doubleValue();
        if (v > 5701 && v <= 8549) {
            return grossSalary
                    .multiply(BigDecimal.valueOf(0.0668))
                    .subtract(BigDecimal.valueOf(380.5))
                    .divide(BigDecimal.valueOf(0.17), new MathContext(2, RoundingMode.HALF_UP));
        } else if (v > 8549 && v < 11141) {
            return grossSalary
                    .multiply(BigDecimal.valueOf(-0.0735))
                    .add(BigDecimal.valueOf(819.08))
                    .divide(BigDecimal.valueOf(0.17), new MathContext(2, RoundingMode.HALF_UP));
        }
        return BigDecimal.ZERO;
    }
}
