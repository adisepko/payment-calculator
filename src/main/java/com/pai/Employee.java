package com.pai;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

@Introspected
public class Employee {

    @NotBlank
    private boolean over26;
    @NotBlank
    private boolean isWorkingInPlaceOfResidence;
    @NotBlank
    private boolean taxReliefEnabled;
    @NotBlank
    private boolean fixedSalary;
    @NotBlank
    private List<BigDecimal> salary;

    public Employee() {

    }

    public boolean isOver26() {
        return over26;
    }

    public void setOver26(boolean over26) {
        this.over26 = over26;
    }

    public boolean isWorkingInPlaceOfResidence() {
        return isWorkingInPlaceOfResidence;
    }

    public void setIsWorkingInPlaceOfResidence(boolean workingInPlaceOfResidence) {
        isWorkingInPlaceOfResidence = workingInPlaceOfResidence;
    }

    public List<BigDecimal> getSalary() {
        return salary;
    }

    public void setSalary(List<BigDecimal> salary) {
        this.salary = salary;
    }

    public boolean isTaxReliefEnabled() {
        return taxReliefEnabled;
    }

    public void setTaxReliefEnabled(boolean taxReliefEnabled) {
        this.taxReliefEnabled = taxReliefEnabled;
    }

    public boolean isFixedSalary() {
        return fixedSalary;
    }

    public void setFixedSalary(boolean fixedSalary) {
        this.fixedSalary = fixedSalary;
    }
}
