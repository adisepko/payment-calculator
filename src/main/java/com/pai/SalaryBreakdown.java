package com.pai;

import io.micronaut.core.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Objects;

//https://poradnikpracownika.pl/-skladki-odprowadzane-od-wynagrodzenia
//https://poradnikpracownika.pl/-nowy-lad-sprawdz-ile-zarobisz
//https://poradnikprzedsiebiorcy.pl/-nowy-polski-lad-jak-wyliczyc-wynagrodzenie-dla-pracownikow
//https://poradnikprzedsiebiorcy.pl/-podwyzszone-kup-pracownika-kiedy-mozna-je-stosowac
//https://www.podatki.gov.pl/polski-lad/kwota-wolna-polski-lad/kalkulator-wynagrodzen-polski-lad/
public class SalaryBreakdown {
    @NonNull
    private final BigDecimal grossSalary;
    @NonNull
    private final SocialInsuranceContribution socialInsuranceContribution;
    @NonNull
    private final BigDecimal healthInsurance;
    @NonNull
    private final BigDecimal taxDeductibleCost;
    @NonNull
    private final BigDecimal taxPrepayment;
    @NonNull
    private final BigDecimal netSalary;

    public SalaryBreakdown(@NonNull SocialInsuranceContribution socialInsuranceContribution,
                           @NonNull BigDecimal healthInsurance,
                           @NonNull BigDecimal grossSalary,
                           @NonNull BigDecimal taxDeductibleCost,
                           @NonNull BigDecimal taxPrepayment,
                           @NonNull BigDecimal netSalary) {
        this.socialInsuranceContribution = socialInsuranceContribution;
        this.healthInsurance = healthInsurance;
        this.grossSalary = grossSalary;
        this.taxDeductibleCost = taxDeductibleCost;
        this.taxPrepayment = taxPrepayment;
        this.netSalary = netSalary;
    }

    @NonNull
    public BigDecimal getGrossSalary() {
        return grossSalary;
    }

    @NonNull
    public SocialInsuranceContribution getSocialInsuranceContribution() {
        return socialInsuranceContribution;
    }

    @NonNull
    public BigDecimal getHealthInsurance() {
        return healthInsurance;
    }

    @NonNull
    public BigDecimal getTaxDeductibleCost() {
        return taxDeductibleCost;
    }

    @NonNull
    public BigDecimal getTaxPrepayment() {
        return taxPrepayment;
    }

    @NonNull
    public BigDecimal getNetSalary() {
        return netSalary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SalaryBreakdown that = (SalaryBreakdown) o;
        return getGrossSalary().equals(that.getGrossSalary())
                && getSocialInsuranceContribution().equals(that.getSocialInsuranceContribution())
                && getHealthInsurance().equals(that.getHealthInsurance())
                && getTaxDeductibleCost().equals(that.getTaxDeductibleCost())
                && getTaxPrepayment().equals(that.getTaxPrepayment())
                && getNetSalary().equals(that.getNetSalary());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGrossSalary(),
                getSocialInsuranceContribution(),
                getHealthInsurance(),
                getTaxDeductibleCost(),
                getTaxPrepayment(), getNetSalary());
    }
}
