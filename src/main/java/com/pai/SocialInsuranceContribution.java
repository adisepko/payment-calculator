package com.pai;

import io.micronaut.core.annotation.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SocialInsuranceContribution {

    @NonNull
    private final BigDecimal pensionContribution;
    @NonNull
    private final BigDecimal disabilityPensionContribution;
    @NonNull
    private final BigDecimal sicknessContribution;
    private static final BigDecimal PENSION_CONTRIBUTION_PERCENT = BigDecimal.valueOf(0.0976);
    private static final BigDecimal DISABILITY_PENSION_CONTRIBUTION_PERCENT = BigDecimal.valueOf(0.015);
    private static final BigDecimal SICKNESS_CONTRIBUTION_PERCENT = BigDecimal.valueOf(0.0245);

    public SocialInsuranceContribution(@NonNull BigDecimal pensionContribution,
                                       @NonNull BigDecimal disabilityPensionContribution, @NonNull BigDecimal sicknessContribution) {
        this.pensionContribution = pensionContribution;
        this.disabilityPensionContribution = disabilityPensionContribution;
        this.sicknessContribution = sicknessContribution;
    }

    public static SocialInsuranceContribution calculate(BigDecimal salary) {
        return new SocialInsuranceContribution(
                salary.multiply(PENSION_CONTRIBUTION_PERCENT).setScale(2, RoundingMode.HALF_UP),
                salary.multiply(DISABILITY_PENSION_CONTRIBUTION_PERCENT).setScale(2, RoundingMode.HALF_UP),
                salary.multiply(SICKNESS_CONTRIBUTION_PERCENT).setScale(2, RoundingMode.HALF_UP)
        );
    }

    public BigDecimal getSocialInsuranceContributionValue() {
        return pensionContribution
                .add(disabilityPensionContribution)
                .add(sicknessContribution)
                .setScale(2, RoundingMode.HALF_UP);
    }

    @NonNull
    public BigDecimal getPensionContribution() {
        return pensionContribution;
    }

    @NonNull
    public BigDecimal getDisabilityPensionContribution() {
        return disabilityPensionContribution;
    }

    @NonNull
    public BigDecimal getSicknessContribution() {
        return sicknessContribution;
    }
}
