package com.pai;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedHashMap;

public class EmployeeRequestHandlerTest {

    private static SalaryBreakdownRequestHandler salaryBreakdownRequestHandler;

    @BeforeAll
    public static void setupServer() {
        salaryBreakdownRequestHandler = new SalaryBreakdownRequestHandler();
    }

    @AfterAll
    public static void stopServer() {
        if (salaryBreakdownRequestHandler != null) {
            salaryBreakdownRequestHandler.getApplicationContext().close();
        }
    }

    @Test
    public void testHandler() {
        LinkedHashMap<String, Object> objectObjectLinkedHashMap = new LinkedHashMap<>();
        objectObjectLinkedHashMap.put("body", "{" +
                "\"over26\": \"true\"," +
                "\"isWorkingInPlaceOfResidence\": \"true\"," +
                "\"taxReliefEnabled\": \"true\"," +
                "\"salary\": 10000}");
        SalaryBreakdown execute = salaryBreakdownRequestHandler.execute(objectObjectLinkedHashMap);
        SalaryBreakdown salaryBreakdown = new SalaryBreakdown(new SocialInsuranceContribution(BigDecimal.valueOf(976),
                BigDecimal.valueOf(150), BigDecimal.valueOf(245)), BigDecimal.valueOf(776.61), BigDecimal.valueOf(10000)
                , BigDecimal.valueOf(250), BigDecimal.valueOf(916), BigDecimal.valueOf(6936.39));
        assertEquals(salaryBreakdown, execute);
    }
}
